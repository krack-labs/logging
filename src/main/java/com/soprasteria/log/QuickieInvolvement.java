package com.soprasteria.log;

import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class QuickieInvolvement{
    
    public String involvement(String id){
        MDC.put("id", id);
        log.info("Quickie's involvement of {}",id);
       
        try{
             // big treatement
            if("error".equals(id)) throw new RuntimeException("Impossible id");
            log.info("Quickie's involvement of {} : SUCCEDED",id);
            MDC.put("id", id);
            this.consolidation();
        }catch(Exception e){
            log.error("Quickie's involvement of {} : FAILED", e);
        }
        
        return "result";
    }


    private void consolidation(){
        log.info("consolidation running");
    }
}
