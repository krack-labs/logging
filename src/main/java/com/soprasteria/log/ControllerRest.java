package com.soprasteria.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ControllerRest {

    private QuickieInvolvement quickieInvolvement;

    @Autowired
    public ControllerRest(QuickieInvolvement quickieInvolvement) {
        this.quickieInvolvement = quickieInvolvement;
    }

    @GetMapping(value = "/")
    public String testQuickieParticipate(@RequestParam String myId) {
        log.debug("call testQuickieParticipate with {}", myId);
        this.quickieInvolvement.involvement(myId);
        return "Thanks to participate "+myId;
    }
}
